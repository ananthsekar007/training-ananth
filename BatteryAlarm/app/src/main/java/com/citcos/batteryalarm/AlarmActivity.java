package com.citcos.batteryalarm;

import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.citcos.batteryalarm.databinding.AlarmActivityBinding;


public class AlarmActivity extends AppCompatActivity implements Animation.AnimationListener, MediaPlayer.OnCompletionListener{
    private Animation bounceAnimation;
    private AlarmActivityBinding binding;
    int n = 1;
    private MediaPlayer mp;

    @Override
    protected void onCreate(Bundle onSavedInstance) {
        super.onCreate(onSavedInstance);
        binding = DataBindingUtil.setContentView(this, R.layout.alarm_activity);
        bounceAnimation = AnimationUtils.loadAnimation(this, R.anim.bounce);
        bounceAnimation.setAnimationListener(this);
        binding.alarmclock.startAnimation(bounceAnimation);
        mp = MediaPlayer.create(getApplicationContext(), R.raw.default_tone);
        if (mp != null) {
            mp.setOnCompletionListener(this);
            mp.start();
        }

    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if (animation == bounceAnimation) {
            binding.alarmclock.startAnimation(bounceAnimation);
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        if (mediaPlayer != null && n <= 3) {
            mediaPlayer.start();
            n++;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mp != null)
            mp.stop();
    }

    public void onFabClicked(View view){
        if (view.getId() == R.id.fab) {
            this.finish();
        }
    }

}