package com.citcos.batteryalarm;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.github.abara.library.batterystats.BatteryStats;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity{

    ImageView icon;
    TextView percent,liveText;
    Button button;
    private FloatingActionButton fab;
    boolean isRunning;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            BatteryStats batteryStats = new BatteryStats(intent);
            int charge=batteryStats.getLevel();

            liveText.setText(batteryStats.getBatteryTechnology()+" • "+batteryStats.getHealthText()+" • "+batteryStats.getTemperatureText(false));

            if(batteryStats.isCharging()){
                icon.setImageDrawable(getResources().getDrawable(R.mipmap.battery_charging));
            }else{
                if (charge <= 35) {
                    icon.setImageDrawable(getResources().getDrawable(R.mipmap.battery_30));
                }else if(charge <=65){
                    icon.setImageDrawable(getResources().getDrawable(R.mipmap.battery_60));
                }else if(charge<=90){
                    icon.setImageDrawable(getResources().getDrawable(R.mipmap.battery_90));
                }else if(charge<=100){
                    icon.setImageDrawable(getResources().getDrawable(R.mipmap.battery_full));
                }

                if(Utils.isServiceRunning(MainActivity.this,BatteryTrackingService.class.getName())) {
                    isRunning = false;
                    Toast.makeText(context, "Service stopped", Toast.LENGTH_SHORT).show();
                    button.setText("Start Alarm Service");
                    stopService(new Intent(MainActivity.this, BatteryTrackingService.class));
                }

            }

            percent.setText(charge+"% • "+(batteryStats.isCharging()?"Charging":"Discharging"));

        }
    };
    private MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        icon=findViewById(R.id.charge_image);
        percent=findViewById(R.id.charge_percent);
        button=findViewById(R.id.button);
        liveText=findViewById(R.id.liveText);

        fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToSettings();
            }
        });

        isRunning=false;
        registerReceiver(broadcastReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

    private void startTrackingService() {
        if(!isRunning) {
            if (Utils.isConnected(this)) {
                Toast.makeText(this, "Service started", Toast.LENGTH_SHORT).show();
                startService(new Intent(this, BatteryTrackingService.class));
                button.setText("Stop Alarm Service");
                isRunning = true;
            }else{
              Toast.makeText(this, "Device is not charging", Toast.LENGTH_SHORT).show();
            }
        }else{
            isRunning=false;
            button.setText("Start Alarm Service");
            stopService(new Intent(this,BatteryTrackingService.class));
        }
    }

    private void goToSettings() {
        Intent settingIntent = new Intent(this, AlarmActivity.class);
        startActivity(settingIntent);
    }

    public void onButtonClicked(View view) {
        startTrackingService();
    }


}
