package com.citcos.batteryalarm;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.IBinder;

import androidx.annotation.Nullable;


public class BatteryTrackingService extends Service {
    private BatterTrackingReceiver batterTrackingReceiver = new BatterTrackingReceiver();

    @Override
    public void onCreate() {
        registerReceiver(batterTrackingReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }

    @Override
    public void onDestroy(){
        unregisterReceiver(batterTrackingReceiver);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }



}
