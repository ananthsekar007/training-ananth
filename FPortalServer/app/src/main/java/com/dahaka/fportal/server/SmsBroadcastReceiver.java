package com.dahaka.fportal.server;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;
import androidx.annotation.NonNull;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;
import java.util.Map;

public class SmsBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = SmsBroadcastReceiver.class.getSimpleName();
    public static final String SMS_CONTENT = "sms_content";
    private String number;
    private String message;


    @Override
    public void onReceive(final Context context, Intent intent) {
        Log.i(TAG, "Intent recieved: " + intent.getAction());

        Bundle bundle = intent.getExtras();
        SmsMessage[] msgs = null;
        if (bundle != null) {
            //---retrieve the SMS message received---
            Object[] pdus = (Object[]) bundle.get("pdus");
            msgs = new SmsMessage[pdus.length];
            for (int i = 0; i < msgs.length; i++) {
                msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                number =msgs[i].getOriginatingAddress();
                message = msgs[i].getMessageBody();
            }

            if(!message.contains("fportal")){
                return;
            }

            final Map<String, Object> map = new HashMap<>();
            map.put("timestamp", ServerValue.TIMESTAMP);
            map.put("message", message);

            if(NetworkUtil.getConnectivityStatus(context)==NetworkUtil.NETWORK_STATUS_NOT_CONNECTED){
                sendSMS(context, number,"Failure, No Network Connection !");
                sendSMS(context, number,"Try again later");
                return;
            }

            FirebaseDatabase.getInstance()
                    .getReference()
                    .child("Messages")
                    .child(number)
                    .push()
                    .setValue(map)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            sendSMS(context, number,"Success");
                            Toast.makeText(context, "Uploaded to database", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            sendSMS(context, number,"Internal Error : "+e.getLocalizedMessage());
                            Toast.makeText(context, "Error : " + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });

        }
    }

    public void sendSMS(Context context,String phoneNo, String msg) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
            Toast.makeText(context, "Message Sent",
                    Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(),
                    Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

}
